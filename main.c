/*
 * TempMonitor.c
 *
 * Created: 12/22/2019 9:55:39 PM
 * Author : Harshana
 * Command : avrdude.exe -c usbasp -p t13 -e -U "flash:w:D:\Creations\AVRC\TempMonitor\Release\TempMonitor.hex:a"
 *         : avrdude.exe -c usbasp -p t13 -e -U "flash:w:D:\Creations\AVRC\TempMonitor\Debug\TempMonitor.hex:a"
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

#define PIN_BUZZ	PB0 // pin 5
#define PIN_INPUT   PB3 // pin 2

#define TSTART		150		// temperature beep start level (ADC level)
#define TGAP		15		// (total gap = 878mV = 5000/256 * TGAP * 3)
#define TNO_CHANGE	250		// No change on temp detection value
#define TINIT		56		// TCNT0 initialize value
#define START_WORK	30	// start working
#define WARN_START	360	// start warning after 6min on no temp change detected
#define WARN_STOP	375	// stop warning as this can be due to very cold temperature

int iTemp = 0;		// temperature reading
int iTCount = 0;	// Timing counter
int iLevel = 2;		// beep level
int iSCount = 0;	// Start delay counter, wait 30sec before start beeping

// ============================================================================
int main(void)
{
    // ------------------------------ init digital pins 
	DDRB |= (1<<PIN_BUZZ);		// Output enable for Buzzer
	
	// ------------------------------ init ADC
	ADMUX = (1<<ADLAR) | (1<<MUX1) | (1<<MUX0);						// left adj, use PB3 input
	ADCSRA = (1<<ADEN) | (1<<ADSC) | (1<<ADPS1) | (1<<ADPS0);		// 1100 0011 = ADC enable, start converting, prescalar 8 (125kHz)
	while (ADCSRA & (1<<ADSC));	// still converting
	
	iTemp = ADCH;

	// ------------------------------ init timer0 (8bit timer)
	TCNT0 = 0;			// not setting TCNT0 during interrupt, full count will result in 220ms timer.

	TCCR0A = 0x00;		// no waveform functionalities
	TCCR0B = (1<<CS02) | (1<<CS00);		// clock/1024 -> 1kHz timer clock.

	TIMSK0 = (1<<TOIE0);				// enable Timer0 overflow interrupt.

	// ------------------------------ watchdog timer
	wdt_enable(WDTO_1S);

	sei();

	while (1)
		sleep_mode();

}

// ============================================================================
ISR(TIM0_OVF_vect)
{
	wdt_reset();
	++iTCount;
	TCNT0 = TINIT;		// let the overflow timer goes little faster than 256ms

	if (iTCount > 5) // look at this every 1 second
	{		
		iTCount = 0;
		iTemp += (ADCH-iTemp)/3;   // change slowly

		if (iTemp < TSTART-TGAP*3)		// add iLevel to get Schmitt trigger effect
			iLevel = 4;
		else if (iTemp < TSTART-TGAP*2)
			iLevel = 3;
		else if (iTemp < TSTART-TGAP*1)
			iLevel = 2;
		else if (iTemp < TSTART)
			iLevel = 1;
		else 
			iLevel = 0;		

		if (iTemp > TNO_CHANGE)		// temp reading not registering
		{
			if (iSCount > WARN_STOP)		// 6.15 minutes
				PORTB |= (1<<PIN_BUZZ);				// off Buzzer (pin High)
			else if (iSCount > WARN_START)	// 6.00 minutes
			{
				iLevel = 3;					// 3 beeps continuous	
				++iSCount;					// if warns, keep counting till WARN_STOP
			}
		}

		if (iSCount < WARN_START+2)		// normally stop counting at WARN_START	
			++iSCount;
	}

	if (iSCount > START_WORK)	// 30 seconds
	{
		if ((iTCount == 0) && (iLevel > 0))
			PORTB &= ~(1<<PIN_BUZZ);				// 1 beep (pin Low)
		else if ((iTCount == 2) && (iLevel > 1))
			PORTB &= ~(1<<PIN_BUZZ);				// 2 beeps
		else if ((iTCount == 4) && (iLevel > 2))
			PORTB &= ~(1<<PIN_BUZZ);				// 3 beeps
		else if (iLevel > 3)
			PORTB &= ~(1<<PIN_BUZZ);				// continuous		
		else
			PORTB |= (1<<PIN_BUZZ);					// off Buzzer (pin High)				
	}
	else
	{
		PORTB |= (1<<PIN_BUZZ);			// off Buzzer (pin High)
	}
}

